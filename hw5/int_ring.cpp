#include <stdio.h>
#include <cstdlib>
#include <mpi.h>
#include <iostream>

double ring_communication(long Nrepeat, long Nsize, MPI_Comm comm) {
  int rank, size;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &size);

  char* msg = (char*) malloc(Nsize);
  for (long i = 0; i < Nsize; i++) msg[i] = 0;

  MPI_Barrier(comm);
  double tt = MPI_Wtime();
  for (long repeat  = 0; repeat < Nrepeat; repeat++) {
    MPI_Status status;
    if (rank == 0) {
      // for (long i = 0; i < Nsize; i++) msg[i] += rank;
      msg[0] += rank;
      MPI_Send(msg, Nsize, MPI_CHAR, 1, repeat, comm);
      MPI_Recv(msg, Nsize, MPI_CHAR, size-1, repeat, comm, &status);
      if (repeat == Nrepeat - 1){
        printf("Value of Messeage: %d \n", msg[0]);
      }
    }
    else if (rank == size-1) {
      MPI_Recv(msg, Nsize, MPI_CHAR, size-2, repeat, comm, &status);
      // for (long i = 0; i < Nsize; i++) msg[i] += rank;
      msg[0] += rank;
      MPI_Send(msg, Nsize, MPI_CHAR, 0, repeat, comm);    
    }
    else{
      MPI_Recv(msg, Nsize, MPI_CHAR, rank-1, repeat, comm, &status);
      // for (long i = 0; i < Nsize; i++) msg[i] += rank;
      msg[0] += rank;
      MPI_Send(msg, Nsize, MPI_CHAR, rank+1, repeat, comm);    
    }
  }
  tt = MPI_Wtime() - tt;

  free(msg);
  return tt;
}

int main(int argc, char** argv) {
  MPI_Init(&argc, &argv);

  if (argc < 1) {
    printf("Usage: mpirun ./ringcommunication Nsize \n");
    abort();
  }
  long Nrepeat = atoi(argv[1]);
  long Nsize = atoi(argv[2]);

  int rank;
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Comm_rank(comm, &rank);

  // long Nrepeat = 1000;
  double tt = ring_communication(Nrepeat, 1, comm);
  if (!rank) printf("ring communication latency: %e ms\n", tt/Nrepeat * 1000);

  // Nrepeat = 10000;
  // long Nsize = 1000000;
  tt = ring_communication(Nrepeat, Nsize, comm);
  if (!rank) printf("ring communication bandwidth: %e GB/s\n", (Nsize*Nrepeat)/tt/1e9);

  MPI_Finalize();
}

