/* Jacobi smoothing to solve -u''=f
 * Global vector has N inner unknowns.
 * g++ -fopenmp -lm jacobi2D-omp.cpp && ./a.out 20 1000
 */ 
#ifdef _OPENMP
  #include <omp.h>
#endif
#include <algorithm>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "utils.h"

#define FWIDTH 3

float filter_Jacobi[FWIDTH][FWIDTH] = {
     0,     0.25,      0,
     0.25,     0,      0.25,
     0,     0.25,      0};

float filter_residual[FWIDTH][FWIDTH] = {
     0,     1,      0,
     1,     -4,      1,
     0,     1,      0};

#define BLOCK_DIM 32
__constant__ float filter_Jacobi_gpu[FWIDTH][FWIDTH];
__constant__ float filter_residual_gpu[FWIDTH][FWIDTH];

__global__ void GPU_Jacobi(float* I, float* I0, const float* F, long Xsize, long Ysize) {
  constexpr long FWIDTH_HALF = (FWIDTH-1)/2;
  __shared__ float smem[BLOCK_DIM+FWIDTH][BLOCK_DIM+FWIDTH];
  long offset_x = blockIdx.x * (BLOCK_DIM-FWIDTH);
  long offset_y = blockIdx.y * (BLOCK_DIM-FWIDTH);

  smem[threadIdx.x][threadIdx.y] = 0;
  if (offset_x + threadIdx.x < Xsize && offset_y + threadIdx.y < Ysize)
    smem[threadIdx.x][threadIdx.y] = I0[(offset_x + threadIdx.x)*Ysize + (offset_y + threadIdx.y)];
  __syncthreads();

  float sum = 0;
  for (long j0 = 0; j0 < FWIDTH; j0++) {
    for (long j1 = 0; j1 < FWIDTH; j1++) {
      sum += smem[threadIdx.x+j0][threadIdx.y+j1] * filter_Jacobi_gpu[j0][j1];
    }
  }

  if (threadIdx.x+FWIDTH < BLOCK_DIM && threadIdx.y+FWIDTH < BLOCK_DIM)
    if (offset_x+threadIdx.x+FWIDTH <= Xsize && offset_y+threadIdx.y+FWIDTH <= Ysize)
      I[(offset_x+threadIdx.x+FWIDTH_HALF)*Ysize + (offset_y+threadIdx.y+FWIDTH_HALF)] = (float)sum + F[(offset_x+threadIdx.x+FWIDTH_HALF)*Ysize + (offset_y+threadIdx.y+FWIDTH_HALF)];
}


__global__ void GPU_Residual(float* I, float* I0, const float* F, long Xsize, long Ysize) {
  constexpr long FWIDTH_HALF = (FWIDTH-1)/2;
  __shared__ float smem[BLOCK_DIM+FWIDTH][BLOCK_DIM+FWIDTH];
  long offset_x = blockIdx.x * (BLOCK_DIM-FWIDTH);
  long offset_y = blockIdx.y * (BLOCK_DIM-FWIDTH);

  smem[threadIdx.x][threadIdx.y] = 0;
  if (offset_x + threadIdx.x < Xsize && offset_y + threadIdx.y < Ysize)
    smem[threadIdx.x][threadIdx.y] = I0[(offset_x + threadIdx.x)*Ysize + (offset_y + threadIdx.y)];
  __syncthreads();

  float sum = 0;
  for (long j0 = 0; j0 < FWIDTH; j0++) {
    for (long j1 = 0; j1 < FWIDTH; j1++) {
      sum += smem[threadIdx.x+j0][threadIdx.y+j1] * filter_residual_gpu[j0][j1];
    }
  }

  if (threadIdx.x+FWIDTH < BLOCK_DIM && threadIdx.y+FWIDTH < BLOCK_DIM)
    if (offset_x+threadIdx.x+FWIDTH <= Xsize && offset_y+threadIdx.y+FWIDTH <= Ysize)
      I[(offset_x+threadIdx.x+FWIDTH_HALF)*Ysize + (offset_y+threadIdx.y+FWIDTH_HALF)] = (float)sum + F[(offset_x+threadIdx.x+FWIDTH_HALF)*Ysize + (offset_y+threadIdx.y+FWIDTH_HALF)];
}


int main(int argc, char * argv[])
{
  int max_N=1024, iter=0, max_iters=100000;

  // sscanf(argv[1], "%d", &max_N);
  // sscanf(argv[2], "%d", &max_iters);
  // sscanf(argv[3], "%d", &max_nthreads);

  // printf("Threads  Size    Time(s) \n");
  for (int N = 32; N <= max_N; N *= 2){

    double time;
    Timer t;
    t.tic();

    /* Allocation of vectors, including left and right ghost points */
    float* U0 = (float *) calloc(sizeof(float), (N)*(N));
    float* U1 = (float *) calloc(sizeof(float), (N)*(N));
    float* Res = (float *) calloc(sizeof(float), (N)*(N));
    float* F = (float *) calloc(sizeof(float), (N)*(N));
    float* F_jacobi = (float *) calloc(sizeof(float), (N)*(N));

    double h = 1.0 / (N + 1);
    double hsq = h*h;
    double invhsq = 1./hsq;
    float res=0, res0=0, tol = 1e-5;

    float filter_residual_h[FWIDTH][FWIDTH];

    for (long i = 0; i < FWIDTH; i++){
      for (long j = 0; j < FWIDTH; j++){
        filter_residual_h[i][j] = invhsq * filter_residual[i][j];
      }
    }
    /* initial residual */
    for (long i = 0; i < (N)*(N); i++) {
      U0[i] = 0; U1[i] = 0; F[i] = 1; F_jacobi[i] = 0.25 * hsq; Res[i] = 0;
    }

    // Allocate GPU memory
    float *U0gpu, *U1gpu, *Resgpu, *F_gpu, *F_jacobi_gpu;
    cudaMalloc(&U0gpu, (N)*(N)*sizeof(float));
    cudaMalloc(&U1gpu, (N)*(N)*sizeof(float));
    cudaMalloc(&Resgpu, (N)*(N)*sizeof(float));
    cudaMalloc(&F_gpu, (N)*(N)*sizeof(float));
    cudaMalloc(&F_jacobi_gpu, (N)*(N)*sizeof(float));
    cudaMemcpy(U0gpu, U0, (N)*(N)*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(U1gpu, U1, (N)*(N)*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(Resgpu, Res, (N)*(N)*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(F_gpu, F, (N)*(N)*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(F_jacobi_gpu, F_jacobi, (N)*(N)*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(filter_Jacobi_gpu, filter_Jacobi, sizeof(filter_Jacobi)); // Initialize filter_gpu
    cudaMemcpyToSymbol(filter_residual_gpu, filter_residual_h, sizeof(filter_residual)); // Initialize filter_gpu
    
    #define BLOCK_DIM 32
    dim3 blockDim(BLOCK_DIM, BLOCK_DIM);
    dim3 gridDim((N)/(BLOCK_DIM-FWIDTH)+1, (N)/(BLOCK_DIM-FWIDTH)+1);
    GPU_Residual<<<gridDim,blockDim, 0>>>(Resgpu, U1gpu, F_gpu, N, N);
    cudaDeviceSynchronize();

    cudaMemcpy(Res, Resgpu, (N)*(N)*sizeof(float), cudaMemcpyDeviceToHost);
    res0 = 0;
    for (long i = 0; i < (N)*(N); i++) res0 = std::max(res0, fabs(Res[i]));
    res = res0;

    //for (iter = 0; iter < max_iters && res/res0 > tol; iter++) {
    for (iter = 0; iter < max_iters; iter++) {

      GPU_Jacobi<<<gridDim,blockDim, 0>>>(U1gpu, U0gpu, F_jacobi_gpu, N, N);

      cudaDeviceSynchronize();
      /* flip pointers; that's faster than memcpy  */
      // memcpy(u,unew,(N)*sizeof(double));
      float* Utemp = U1gpu;
      U0gpu = U1gpu;
      U1gpu = Utemp;
      // if (0 == (iter % 10000)) {
      //   GPU_Residual<<<gridDim,blockDim, 0>>>(Resgpu, U1gpu, F_gpu, N, N);
      //   cudaDeviceSynchronize();

      //   cudaMemcpy(Res, Resgpu, (N)*(N)*sizeof(float), cudaMemcpyDeviceToHost);
      //   res = 0;
      //   for (long i = 0; i < (N)*(N); i++) res = std::max(res, fabs(Res[i]));
      //   printf("Iter %d: Residual: %g\n", iter, res);
      // }
    }

    /* Clean up */
    cudaFree(U0gpu);
    cudaFree(U1gpu);
    cudaFree(F_gpu);
    cudaFree(F_jacobi_gpu);
    cudaFree(Resgpu);
    free(U0);
    free(U1);
    free(Res);
    free(F);
    free(F_jacobi);

    /* timing */
    time = t.toc();
    printf("%4d    %10f \n", N, time);
  }
  return 0;
}