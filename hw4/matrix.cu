#include <algorithm>
#include <stdio.h>
#include <omp.h>
#include <string>

void inner_product(double* sum_ptr, const double* a, const double* b, long N){
  double sum = 0;
  #pragma omp parallel for schedule(static) reduction(+:sum)
  for (long i = 0; i < N; i++) sum += a[i] * b[i];
  *sum_ptr = sum;
}

void MvMult0(long m, long n, const double* a, const double* b, double* c) {
  for (long i = 0; i < m; i++){
    inner_product(c+i, a+i*m, b, n);
  }
}

void Check_CUDA_Error(const char *message){
  cudaError_t error = cudaGetLastError();
  if(error!=cudaSuccess) {
    fprintf(stderr,"ERROR: %s: %s\n", message, cudaGetErrorString(error) );
    exit(-1);
  }
}

#define BLOCK_SIZE 1024

__global__ void reduction_kernel(double* sum, const double* a, long N){
  __shared__ double smem[BLOCK_SIZE];
  int idx = (blockIdx.x) * blockDim.x + threadIdx.x;

  if (idx < N) smem[threadIdx.x] = a[idx];
  else smem[threadIdx.x] = 0;

  __syncthreads();
  if (threadIdx.x < 512) smem[threadIdx.x] += smem[threadIdx.x + 512];
  __syncthreads();
  if (threadIdx.x < 256) smem[threadIdx.x] += smem[threadIdx.x + 256];
  __syncthreads();
  if (threadIdx.x < 128) smem[threadIdx.x] += smem[threadIdx.x + 128];
  __syncthreads();
  if (threadIdx.x <  64) smem[threadIdx.x] += smem[threadIdx.x +  64];
  __syncthreads();
  if (threadIdx.x <  32) {
    smem[threadIdx.x] += smem[threadIdx.x +  32];
    __syncwarp();
    smem[threadIdx.x] += smem[threadIdx.x +  16];
    __syncwarp();
    smem[threadIdx.x] += smem[threadIdx.x +   8];
    __syncwarp();
    smem[threadIdx.x] += smem[threadIdx.x +   4];
    __syncwarp();
    smem[threadIdx.x] += smem[threadIdx.x +   2];
    __syncwarp();
    if (threadIdx.x == 0) sum[blockIdx.x] = smem[0] + smem[1];
  }
}

__global__ void inner_product_kernel(double* sum, const double* a, const double* b, long N){
  __shared__ double smem[BLOCK_SIZE];
  int idx = (blockIdx.x) * blockDim.x + threadIdx.x;

  if (idx < N) smem[threadIdx.x] = a[idx] * b[idx];
  else smem[threadIdx.x] = 0;

  __syncthreads();
  if (threadIdx.x < 512) smem[threadIdx.x] += smem[threadIdx.x + 512];
  __syncthreads();
  if (threadIdx.x < 256) smem[threadIdx.x] += smem[threadIdx.x + 256];
  __syncthreads();
  if (threadIdx.x < 128) smem[threadIdx.x] += smem[threadIdx.x + 128];
  __syncthreads();
  if (threadIdx.x <  64) smem[threadIdx.x] += smem[threadIdx.x +  64];
  __syncthreads();
  if (threadIdx.x <  32) {
    smem[threadIdx.x] += smem[threadIdx.x +  32];
    __syncwarp();
    smem[threadIdx.x] += smem[threadIdx.x +  16];
    __syncwarp();
    smem[threadIdx.x] += smem[threadIdx.x +   8];
    __syncwarp();
    smem[threadIdx.x] += smem[threadIdx.x +   4];
    __syncwarp();
    smem[threadIdx.x] += smem[threadIdx.x +   2];
    __syncwarp();
    if (threadIdx.x == 0) sum[blockIdx.x] = smem[0] + smem[1];
  }
}

int main() {
  long N = (1UL<<25);
  long M = 3;

  double *x, *y, *sum_ref, *sum;
  cudaMallocHost((void**)&x, M*N * sizeof(double));
  cudaMallocHost((void**)&y, N * sizeof(double));
  cudaMallocHost((void**)&sum_ref, M * sizeof(double));
  cudaMallocHost((void**)&sum, M * sizeof(double));

  #pragma omp parallel for schedule(static)
  for (long i = 0; i < M*N; i++) x[i] = 1; 

  #pragma omp parallel for schedule(static)
  for (long i = 0; i < N; i++) y[i] = 1;

  double tt = omp_get_wtime();
  MvMult0(M, N, x, y, sum_ref);
  printf("CPU Bandwidth = %f GB/s\n", M*N*sizeof(double) / (omp_get_wtime()-tt)/1e9);

  double *x_d, *y_d, *z_d;
  double s_d;
  cudaMalloc(&x_d, M*N*sizeof(double)); cudaMalloc(&y_d, N*sizeof(double));
  cudaMalloc((void**)&s_d, M*sizeof(double));
  long N_work = 1;
  for (long i = (N+BLOCK_SIZE-1)/(BLOCK_SIZE); i > 1; i = (i+BLOCK_SIZE-1)/(BLOCK_SIZE)) N_work += i;
  cudaMalloc(&z_d, N_work*sizeof(double)); // extra memory buffer for reduction across thread-blocks

  cudaMemcpyAsync(x_d, x, M*N*sizeof(double), cudaMemcpyHostToDevice);
  cudaDeviceSynchronize();
  cudaMemcpyAsync(y_d, y, N*sizeof(double), cudaMemcpyHostToDevice);
  cudaDeviceSynchronize();
  tt = omp_get_wtime();


  for(long i = 0; i < M; i++){
    double* sum_d = z_d;
    long Nb = (N+BLOCK_SIZE-1)/(BLOCK_SIZE);
    inner_product_kernel<<<Nb,BLOCK_SIZE>>>(sum_d, x_d + i*N, y_d, N);
    while (Nb > 1) {
      long N = Nb;
      Nb = (Nb+BLOCK_SIZE-1)/(BLOCK_SIZE);
      reduction_kernel<<<Nb,BLOCK_SIZE>>>(sum_d + N, sum_d, N);
      sum_d += N;
    }
    cudaMemcpyAsync(&sum[i], sum_d, 1*sizeof(double), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
  }


  // cudaMemcpyAsync(&sum, &s_d, M*sizeof(double), cudaMemcpyDeviceToHost);
  // cudaDeviceSynchronize();
  printf("GPU Bandwidth = %f GB/s\n", M*N*sizeof(double) / (omp_get_wtime()-tt)/1e9);
  double max_err = 0;
  for (long i = 0; i < M; i++) max_err = std::max(max_err, fabs(sum[i] - sum_ref[i]));
  printf("Error = %f\n", max_err);
  cudaFree(x_d);
  cudaFree(y_d);
  cudaFree(z_d);
  cudaFreeHost(x);

  return 0;
}

