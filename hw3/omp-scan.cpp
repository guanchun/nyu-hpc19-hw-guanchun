#include <algorithm>
#include <stdio.h>
#include <math.h>
#include <omp.h>

// Scan A array and write result into prefix_sum array;
// use long data type to avoid overflow
void scan_seq(long* prefix_sum, const long* A, long n) {
  if (n == 0) return;
  prefix_sum[0] = A[0];
  for (long i = 1; i < n; i++) {
    prefix_sum[i] = prefix_sum[i-1] + A[i];
  }
}

void scan_omp(long* prefix_sum, const long* A, long n) {
  // TODO: implement multi-threaded OpenMP scan
    int nthreads = 8;
    int N_thread = n / nthreads;
    long* S = (long*) malloc(nthreads * sizeof(long));
    long* PS = (long*) malloc(nthreads * sizeof(long));
    #ifdef _OPENMP
      omp_set_num_threads(nthreads);
    #endif
    #pragma omp parallel for
        // Update the red points
        for (int j = 0; j < nthreads; j++){
          scan_seq(&prefix_sum[j*N_thread], &A[j*N_thread], N_thread);
          S[j] = prefix_sum[(j+1)*N_thread - 1];
        }
    #pragma omp barrier
    scan_seq(PS, S, nthreads);
    // printf("%d ",N_thread);
    //#pragma omp parallel for
    for (int j = 1; j < nthreads; j++){
      for (int k = 0; k < N_thread; k++){
          prefix_sum[j*N_thread + k] += PS[j-1];
        }
      }
    #pragma omp barrier
}

int main() {
  long N = 100000000;
  long NREPEATS = 1;
  long* A = (long*) malloc(N * sizeof(long));
  long* B0 = (long*) malloc(N * sizeof(long));
  long* B1 = (long*) malloc(N * sizeof(long));
  for (long i = 0; i < N; i++) A[i] = rand();

  double tt = omp_get_wtime();
  for (long rep = 0; rep < NREPEATS; rep++){
    scan_seq(B0, A, N);
  }
  printf("sequential-scan = %fs\n", (omp_get_wtime() - tt) / NREPEATS);

  tt = omp_get_wtime();
  for (long rep = 0; rep < NREPEATS; rep++){
    scan_omp(B1, A, N);
  }
  printf("parallel-scan   = %fs\n", (omp_get_wtime() - tt) / NREPEATS);

  long err = 0;
  for (long i = 0; i < N; i++) err = std::max(err, std::abs(B0[i] - B1[i]));
  printf("error = %ld\n", err);

  free(A);
  free(B0);
  free(B1);
  return 0;
}
