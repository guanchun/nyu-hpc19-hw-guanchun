// Parallel sample sort
#include <stdio.h>
#include <unistd.h>
#include <mpi.h>
#include <stdlib.h>
#include <algorithm>

int main( int argc, char *argv[]) {
  MPI_Init(&argc, &argv);

  // Number of random numbers per processor (this should be increased
  // for actual tests or could be passed in through the command line
  if (argc < 1) {
    printf("Usage: mpirun ./ssort Nsize \n");
    abort();
  }
  int N = atoi(argv[1]);

  int rank, p;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  if (rank == 0){
    printf("Size N = %d on %d threads \n", N, p);
  }

  int* vec = (int*)malloc(N*sizeof(int));
  // seed random number generator differently on every core
  srand((unsigned int) (rank + 393));

  // fill vector with random integers
  for (int i = 0; i < N; ++i) {
    vec[i] = rand();
  }
  // printf("rank: %d, first entry: %d\n", rank, vec[0]);
  /* timing */
  MPI_Barrier(MPI_COMM_WORLD);
  double tt = MPI_Wtime();

  // sort locally
  std::sort(vec, vec+N);

  // sample p-1 entries from vector as the local splitters, i.e.,
  // every N/P-th entry of the sorted vector
  int* local_samples = (int*)malloc((p-1)*sizeof(int));
  for (int i = 0; i < p-1; i++){
    int ind = (int)((i+1)*N / p);
    local_samples[i] = vec[ind];
  }
  
  // every process communicates the selected entries to the root
  // process; use for instance an MPI_Gather
  int* all_samples = (int*)malloc(p*(p-1)*sizeof(int));
  MPI_Gather(local_samples, p-1, MPI_INT, all_samples, p-1, MPI_INT, 0, MPI_COMM_WORLD);

  // root process does a sort and picks (p-1) splitters (from the
  // p(p-1) received elements)
  int* splitters = (int*)malloc((p-1)*sizeof(int));
  if (rank == 0){
    std::sort(all_samples, all_samples+p*(p-1));    
    for (int i = 0; i < p-1; i++){
      splitters[i] = all_samples[(i+1)*(p-1)];
    }
  }
  
  // root process broadcasts splitters to all other processes
  MPI_Bcast(splitters, p-1, MPI_INT, 0, MPI_COMM_WORLD);

  // every process uses the obtained splitters to decide which
  // integers need to be sent to which other process (local bins).
  // Note that the vector is already locally sorted and so are the
  // splitters; therefore, we can use std::lower_bound function to
  // determine the bins efficiently.
  // Hint: the MPI_Alltoallv exchange in the next step requires
  // send-counts and send-displacements to each process. Determining the
  // bins for an already sorted array just means to determine these
  // counts and displacements. For a splitter s[i], the corresponding
  // send-displacement for the message to process (i+1) is then given by,
  // sdispls[i+1] = std::lower_bound(vec, vec+N, s[i]) - vec;
  
  int* sdispls = (int*)malloc(p*sizeof(int)); 
  sdispls[0] = 0;
  for (int i = 1; i < p; i++){
    sdispls[i] = std::lower_bound(vec, vec+N, splitters[i-1]) - vec;
  }

  int* sendCounts = (int*)malloc(p*sizeof(int));
  int* recvCounts = (int*)malloc(p*sizeof(int));
  for (int i = 0; i < p-1; i++){
    sendCounts[i] = sdispls[i+1] - sdispls[i];
  }
  sendCounts[p-1] = N - sdispls[p-1];
  // printf("rank %d sendCounts %d %d %d %d \n", rank, sendCounts[0], sendCounts[1], sendCounts[2], sendCounts[3]);
  
  // send and receive: first use an MPI_Alltoall to share with every
  // process how many integers it should expect, and then use
  // MPI_Alltoallv to exchange the data
  MPI_Alltoall(sendCounts,1,MPI_INT, recvCounts,1,MPI_INT,MPI_COMM_WORLD);

  int* rdispls = (int*)malloc(p*sizeof(int)); 
  rdispls[0] = 0;
  for (int i = 1; i < p; i++){
    rdispls[i] = rdispls[i-1] + recvCounts[i-1];
  }
  int N_recv = rdispls[p-1] + recvCounts[p-1];
  int* recv_vec = (int*)malloc(N_recv*sizeof(int));

  // printf("rank %d N_recv %d recvCounts %d %d %d %d \n", rank, N_recv, recvCounts[0], recvCounts[1], recvCounts[2], recvCounts[3]);
  // printf("rank %d N_recv %d \n", rank, N_recv);

  MPI_Alltoallv(vec, sendCounts, sdispls, MPI_INT,
                recv_vec, recvCounts, rdispls, MPI_INT, MPI_COMM_WORLD);

  // do a local sort of the received data
  std::sort(recv_vec, recv_vec+N_recv);

  /* timing */
  MPI_Barrier(MPI_COMM_WORLD);
  double elapsed = MPI_Wtime() - tt;
  if (0 == rank) {
    printf("Time elapsed is %f seconds.\n", elapsed);
  }

  // every process writes its result to a file
  { // Write output to a file
    FILE* fd = NULL;
    char filename[256];
    snprintf(filename, 256, "output%02d.txt", rank);
    fd = fopen(filename,"w+");

    if(NULL == fd) {
      printf("Error opening file \n");
      return 1;
    }

    fprintf(fd, "rank %d bucket sort result %d number \n", rank, N_recv);
    for(int n = 0; n < N_recv; ++n){
      fprintf(fd, "  %d\n", recv_vec[n]);
    }
    fclose(fd);
  }

  free(vec);
  free(recv_vec);
  free(splitters);
  free(sendCounts);
  free(recvCounts);
  free(sdispls);
  free(rdispls);

  MPI_Finalize();
  return 0;
}
