#!/bin/bash
#
##SBATCH --nodes=1
#SBATCH --nodes=8
#SBATCH --ntasks-per-node=8
##SBATCH --cpus-per-task=2
#SBATCH --time=4:00:00
#SBATCH --mem=64GB
#SBATCH --job-name=myHPC_hw6
#SBATCH --mail-type=END
##SBATCH --mail-user=gl1705@nyu.edu
#SBATCH --output=slurm_%j.out

module load openmpi/intel/3.1.3

make

mpirun -np 1 ./jacobi2D-mpi 128 10000

mpirun -np 4 ./jacobi2D-mpi 256 10000

mpirun -np 16 ./jacobi2D-mpi 512 10000

mpirun -np 64 ./jacobi2D-mpi 1024 10000

mpirun -np 64 ./jacobi2D-mpi 4096 10000

mpirun -np 16 ./jacobi2D-mpi 4096 10000

mpirun -np 4 ./jacobi2D-mpi 4096 10000

mpirun -np 1 ./jacobi2D-mpi 4096 10000

mpirun -np 1 ./jacobi2D-mpi-nonblocking 128 10000

mpirun -np 4 ./jacobi2D-mpi-nonblocking 256 10000

mpirun -np 16 ./jacobi2D-mpi-nonblocking 512 10000

mpirun -np 64 ./jacobi2D-mpi-nonblocking 1024 10000

mpirun -np 64 ./jacobi2D-mpi-nonblocking 4096 10000

mpirun -np 16 ./jacobi2D-mpi-nonblocking 4096 10000

mpirun -np 4 ./jacobi2D-mpi-nonblocking 4096 10000

mpirun -np 1 ./jacobi2D-mpi-nonblocking 4096 10000

mpirun -np 64 ./ssort 10000

mpirun -np 64 ./ssort 100000

mpirun -np 64 ./ssort 1000000
