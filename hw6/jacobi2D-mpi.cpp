/* MPI-parallel Jacobi smoothing to solve -u''=f
 * Global vector has N unknowns, each processor works with its
 * part, which has lN = N/p unknowns.
 * Author: Georg Stadler
 */
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <string.h>

/* compuate global residual, assuming ghost values are updated */
double compute_residual(double *lu, int lN, double invhsq){
  int i, j;
  double tmp, gres = 0.0, lres = 0.0;

  for (i = 1; i <= lN; i++){
    for (j = 1; j <= lN; j++){
      tmp = invhsq * (4*lu[i+j*(lN+2)] - lu[i-1+j*(lN+2)] - lu[i+1+j*(lN+2)] - lu[i+(j-1)*(lN+2)] - lu[i+(j+1)*(lN+2)]) - 1;
      lres += tmp * tmp;
    }
  }
  /* use allreduce for convenience; a reduce would also be sufficient */
  MPI_Allreduce(&lres, &gres, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  return sqrt(gres);
}


int main(int argc, char * argv[]){
  int mpirank, i, j, p, N, lN, iter, max_iters;
  MPI_Status status, status1;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
  MPI_Comm_size(MPI_COMM_WORLD, &p);

  /* get name of host running MPI process */
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);
  // printf("Rank %d/%d running on %s.\n", mpirank, p, processor_name);

  sscanf(argv[1], "%d", &N);
  sscanf(argv[2], "%d", &max_iters);
  if (mpirank == 0){
    printf("Size N = %d running %d iterations with %d threads \n", N, max_iters, p);
  }
  /* compute number of unknowns handled by each process */
  int sp = (int)(sqrt(p));
  lN = N / sp;
  if ((N % p != 0) && mpirank == 0 ) {
    printf("N: %d, local N: %d\n", N, lN);
    printf("Exiting. N must be a multiple of p\n");
    MPI_Abort(MPI_COMM_WORLD, 0);
  }
  /* timing */
  MPI_Barrier(MPI_COMM_WORLD);
  double tt = MPI_Wtime();

  /* Allocation of vectors, including left/upper and right/lower ghost points */
  double * lu    = (double *) calloc(sizeof(double), (lN + 2)*(lN + 2));
  double * lunew = (double *) calloc(sizeof(double), (lN + 2)*(lN + 2));
  double * lu_left = (double *) calloc(sizeof(double), lN);
  double * lu_right = (double *) calloc(sizeof(double), lN);
  double * lu_up = (double *) calloc(sizeof(double), lN);
  double * lu_down = (double *) calloc(sizeof(double), lN);
  double * lunew_left = (double *) calloc(sizeof(double), lN);
  double * lunew_right = (double *) calloc(sizeof(double), lN);
  double * lunew_up = (double *) calloc(sizeof(double), lN);
  double * lunew_down = (double *) calloc(sizeof(double), lN);
  double * lutemp;

  double h = 1.0 / (N + 1);
  double hsq = h * h;
  double invhsq = 1./hsq;
  double gres, gres0, tol = 1e-5;

  /* initial residual */
  gres0 = compute_residual(lu, lN, invhsq);
  gres = gres0;

  for (iter = 0; iter < max_iters; iter++) {
  // for (iter = 0; iter < max_iters && gres/gres0 > tol; iter++) {

    /* Jacobi step for the 'ghost' points first */
    for (i = 1; i <= lN; i++){
      lunew[i+1*(lN+2)]= 0.25 * (hsq + lu[i-1+1*(lN+2)] + lu[i+1+1*(lN+2)] + lu[i] + lu[i+2*(lN+2)]);
      lu_down[i-1] = lunew[i+1*(lN+2)];
      lunew[i+lN*(lN+2)]= 0.25 * (hsq + lu[i-1+lN*(lN+2)] + lu[i+1+lN*(lN+2)] + lu[i+(lN-1)*(lN+2)] + lu[i+(lN+1)*(lN+2)]);
      lu_up[i-1] = lunew[i+lN*(lN+2)];
    }
    lu_left[0] = lu_down[0]; lu_right[0] = lu_down[lN-1];
    lu_left[lN-1] = lu_up[0]; lu_right[lN-1] = lu_up[lN-1];
    for (j = 2; j <= lN-1; j++){
      lunew[1+j*(lN+2)]= 0.25 * (hsq + lu[j*(lN+2)] + lu[2+j*(lN+2)] + lu[1+(j-1)*(lN+2)] + lu[1+(j+1)*(lN+2)]);
      lu_left[j-1] = lunew[1+j*(lN+2)];
      lunew[lN+j*(lN+2)]= 0.25 * (hsq + lu[lN-1+j*(lN+2)] + lu[lN+1+j*(lN+2)] + lu[lN+(j-1)*(lN+2)] + lu[lN+(j+1)*(lN+2)]);
      lu_right[j-1] = lunew[lN+j*(lN+2)];
    }

    /* Jacobi step for local points */
    for (i = 2; i <= lN-1; i++){
      for (j = 2; j <= lN-1; j++){  
        lunew[i+j*(lN+2)]= 0.25 * (hsq + lu[i-1+j*(lN+2)] + lu[i+1+j*(lN+2)] + lu[i+(j-1)*(lN+2)] + lu[i+(j+1)*(lN+2)]);
      }  
    }

    /* communicate ghost values */
    if (mpirank < p - sp) {
      /* If not the process for the top blocks, send/recv bdry values to the up blocks */
      MPI_Recv(lunew_up, lN, MPI_DOUBLE, mpirank+sp, 123, MPI_COMM_WORLD, &status);
      MPI_Send(lu_up, lN, MPI_DOUBLE, mpirank+sp, 124, MPI_COMM_WORLD);      
    }
    if (mpirank >= sp) {
      /* If not the process for the bottom blocks, send/recv bdry values to the down blocks */
      MPI_Send(lu_down, lN, MPI_DOUBLE, mpirank-sp, 123, MPI_COMM_WORLD);
      MPI_Recv(lunew_down, lN, MPI_DOUBLE, mpirank-sp, 124, MPI_COMM_WORLD, &status1);                        
    }
    if ((mpirank % sp) > 0) {
      /* If not the process for the left blocks, send/recv bdry values to the left blocks */
      MPI_Recv(lunew_left, lN, MPI_DOUBLE, mpirank-1, 125, MPI_COMM_WORLD, &status);
      MPI_Send(lu_left, lN, MPI_DOUBLE, mpirank-1, 126, MPI_COMM_WORLD);      
    }
    if ((mpirank % sp) < sp - 1) {
      /* If not the process for the right blocks, send/recv bdry values to the right blocks */
      MPI_Send(lu_right, lN, MPI_DOUBLE, mpirank+1, 125, MPI_COMM_WORLD);  
      MPI_Recv(lunew_right, lN, MPI_DOUBLE, mpirank+1, 126, MPI_COMM_WORLD, &status1);          
    }

    /* Copy the new ghost points value to lunew */
    for (i = 1; i <= lN; i++){
      lunew[i] = lunew_down[i-1];
      lunew[i+(lN+1)*(lN+2)] = lunew_up[i-1];
      lunew[i*(lN+2)] = lunew_left[i-1];
      lunew[(lN+1)+i*(lN+2)] = lunew_right[i-1];
    }

    /* copy newu to u using pointer flipping */
    lutemp = lu; lu = lunew; lunew = lutemp;
    if (iter == max_iters - 1) {
      gres = compute_residual(lu, lN, invhsq);
      if (0 == mpirank) {
	       printf("Iter %d: Residual: %g\n", iter, gres);
      }
    }
  }

  /* Clean up */
  free(lu);
  free(lunew);

  /* timing */
  MPI_Barrier(MPI_COMM_WORLD);
  double elapsed = MPI_Wtime() - tt;
  if (0 == mpirank) {
    printf("Time elapsed is %f seconds.\n", elapsed);
  }
  MPI_Finalize();
  return 0;
}
