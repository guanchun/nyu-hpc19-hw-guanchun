/* Jacobi smoothing to solve -u''=f
 * Global vector has N inner unknowns.
 * g++ -fopenmp -lm gs2D-omp.cpp && ./a.out 20 1000
 */ 
#ifdef _OPENMP
   #include <omp.h>
#else
   #define omp_get_num_threads() 1
#endif
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "utils.h"



/* compuate global residual, assuming ghost values are updated */
double compute_residual(double *u, double *f, int N, double invhsq)
{
  int i, j;
  double tmp, res = 0.0;

  for (j = 1; j <= N; j++){
    for (i = 1; i <= N; i++){
      tmp =  f[i+j*N] - invhsq * (4*u[i+j*N] - u[i-1+j*N] - u[i+1+j*N] - u[i+(j-1)*N] - u[i+(j+1)*N]);
      res += tmp * tmp;
    }   
  }
  return sqrt(res);
}


int main(int argc, char * argv[])
{
  int i, max_N, iter=0, max_iters, max_nthreads;

  sscanf(argv[1], "%d", &max_N);
  sscanf(argv[2], "%d", &max_iters);
  sscanf(argv[3], "%d", &max_nthreads);

  #ifdef _OPENMP
    int nthreads = 2;
  #else
    max_nthreads = 2;
  #endif

  printf("Threads  Size    Time(s) \n");
  for (int N = 32; N <= max_N; N *= 2){
    for (int nthreads = 2; nthreads <= max_nthreads; nthreads++){
      /* timing */
      // double t = omp_get_wtime();
      double time;
      Timer t;
      t.tic();

      /* Allocation of vectors, including left and right ghost points */
      double* u    = (double *) calloc(sizeof(double), (N+2)*(N+2));
      double* unew = (double *) calloc(sizeof(double), (N+2)*(N+2));
      double* f = (double *) calloc(sizeof(double), (N+2)*(N+2));

      double h = 1.0 / (N + 1);
      double hsq = h*h;
      double invhsq = 1./hsq;
      double res, res0, tol = 1e-8;

      /* initial residual */
      for (long i = 0; i < (N+2)*(N+2); i++) {
        u[i] = 0; unew[i] = 0; f[i] = 1;
      }
      res0 = compute_residual(u, f, N, invhsq);
      res = res0;
      double omega = 1.0; //2./3;

      #ifdef _OPENMP
        omp_set_num_threads(nthreads);
      #endif

      //for (iter = 0; iter < max_iters && res/res0 > tol; iter++) {
      for (iter = 0; iter < max_iters; iter++) {

        /* GS step for all the inner points */
        #pragma omp parallel for
        // Update the red points
        for (int j = 1; j <= N; j++){
          for (int i = (2-(j%2)); i <= N; i+=2){
            unew[i+j*N] =  0.25 * (hsq * f[i+j*N] + u[i-1+j*N] + u[i+1+j*N] + u[i+(j-1)*N] + u[i+(j+1)*N]);
          }
        }

        #pragma omp barrier

        double* utemp = unew;

        #pragma omp parallel for
        // Update the black points
        for (int j = 1; j <= N; j++){
          for (int i = (1+(j%2)); i <= N; i+=2){
            unew[i+j*N] =  0.25 * (hsq * f[i+j*N] + utemp[i-1+j*N] + utemp[i+1+j*N] + utemp[i+(j-1)*N] + utemp[i+(j+1)*N]);
          }
        }

        #pragma omp barrier
        /* flip pointers; that's faster than memcpy  */
        // memcpy(u,unew,(N+2)*sizeof(double));
        utemp = u;
        u = unew;
        unew = utemp;
        if (0 == (iter % 10000)) {
          res = compute_residual(u, f, N, invhsq);
          //printf("Iter %d: Residual: %g\n", iter, res);
        }
      }
      //}

      /* Clean up */
      free(u);
      free(unew);
      free(f);

      /* timing */
      //ime = omp_get_wtime() - t;
      time = t.toc();
      printf("%4d    %4d    %10f \n", nthreads, N, time);
    }
  }
  return 0;
}